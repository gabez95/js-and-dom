/**
 Subraya (manipulando el DOM) todas las palabras de los párrafos en "ejercicio.html" que contengan más de 5 letras.
 * Ejercicio 6
 */

const paragraphs = document.querySelectorAll('p');

for (const paragraph of paragraphs) {
  const longWords = Array.from(paragraph.innerText.match(/([A-Z])\w{5,}/gim));

  for (const word of longWords) {
    const uWord = `<u>${word}</u>`;
    paragraph.innerHTML = paragraph.innerHTML.replaceAll(word, uWord);
  }
}
