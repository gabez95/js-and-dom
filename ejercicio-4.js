/**
 * Ejercicio 4
Escribe una función que, al recibir un array como parámetro, elimine los strings repetidos del mismo.
No se permite hacer uso de Set ni Array.from().
// Ejemplo
const names = [
    'A-Jay',
    'Manuel',
    'Manuel',
    'Eddie',
    'A-Jay',
    'Su',
    'Reean',
    'Manuel',
    'A-Jay',
    'Zacharie',
    'Zacharie',
    'Tyra',
    'Rishi',
    'Arun',
    'Kenton',
];
 */

const names = [
  'A-Jay',
  'Manuel',
  'Manuel',
  'Eddie',
  'A-Jay',
  'Su',
  'Reean',
  'Manuel',
  'A-Jay',
  'Zacharie',
  'Zacharie',
  'Tyra',
  'Rishi',
  'Arun',
  'Kenton',
];

// reduce

const reducedNames = names.reduce((acc, name) => {
  acc.includes(name) ? acc : acc.push(name);
  return acc;
}, []);

console.log(reducedNames);

// iteration

for (let i = 0; i < names.length; i++) {
  let counter = 0;
  for (let j = 0; j < names.length; j++) {
    if (names[i] === names[j]) counter++;
  }
  if (counter > 1) {
    for (let k = 1; k < counter; k++) {
      const position = names.lastIndexOf(names[i]);
      names.splice(position, 1);
    }
  }
}

console.log(names);

// iteration simpler

// const newArray = [];
// for (let index = 0; index < array.length; index++) {
//   if (newArray.indexOf(array[index]) === -1) {
//     newArray.push(array[index]);
//   }
// }
