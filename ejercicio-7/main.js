const squareArea = document.querySelector('.square-area');
const button = document.querySelector('button');

// random rgb generator
const getRandomRGB = () => {
    const getRandom = () => Math.floor(Math.random() * 256);

    return `rgb(${getRandom()}, ${getRandom()}, ${getRandom()})`;
};

// First load
window.addEventListener('load', () => {
    const squares = document.querySelectorAll('div');
    for (const square of squares) {
        square.style.background = getRandomRGB();
    }
});

// Refresh interval
setInterval(() => {
    const squares = document.querySelectorAll('div');
    for (const square of squares) {
        square.style.background = getRandomRGB();
    }
}, 1000);

button.addEventListener('click', (e) => {
    e.preventDefault();
    const newDiv = document.createElement('div');
    newDiv.style.backgroundColor = `${getRandomRGB()}`;
    squareArea.append(newDiv);
});
