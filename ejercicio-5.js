/**
 * Ejercicio 5
Consigue una lista con los nombres de los personajes de la serie Rick and Morty que aparecen en los episodios lanzados en el mes de enero (el año no importa).
Utiliza llamadas a la API: 'https://rickandmortyapi.com/api/'
 */

const getEpisodes = async () => {
  try {
    let episodesArr = [];
    let charactersArr = [];

    // Episode pages in api
    const response = await fetch('https://rickandmortyapi.com/api/episode');
    const { info } = await response.json();
    const episodesPages = info.pages;

    // Getting all episodes from the whole of the pages
    for (let i = 1; i <= episodesPages; i++) {
      const response = await fetch(
        `https://rickandmortyapi.com/api/episode?page=${i}`
      );
      const { results } = await response.json();
      episodesArr = episodesArr.concat(results);
    }

    // filtering January episodes
    const janEpisodesArr = episodesArr.filter((episode) =>
      episode.air_date.includes('January')
    );

    //Getting a list of all characters from episodes casted in January
    for (const episode of janEpisodesArr) {
      for (const character of episode.characters) {
        const response = await fetch(`${character}`);
        const { name } = await response.json();

        charactersArr.includes(name) ? charactersArr : charactersArr.push(name);
      }
    }
    return charactersArr;
  } catch (error) {
    console.log(error);
  }
};

console.log(getEpisodes());
