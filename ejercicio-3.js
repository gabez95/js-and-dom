/**
 * Ejercicio 3
Crea un programa que reciba un número en decimal o binario y devuelva la conversión:


Si le pasamos un nº en decimal debe retornar la conversión a binario.


Si le pasamos un nº en binario deberá devolver su equivalente decimal.


Para ello la función deberá recibir un segundo parámetro que indique la base: 10 (decimal) o 2 (binario).
No se permite utilizar el método parseInt().
 */

const conversion = (number, base = 10) => {
  if (base === 10) {
    //metodo A
    return (number >>> 0).toString(2);

    //metodo B
    // const arr = [];
    // while (true) {
    //   const result = Math.floor(number / 2);
    //   const remainder = Math.floor(number % 2);
    //   arr.unshift(remainder);
    //   number = result;
    //   if (result == 0) break;
    // }
    // return Number(arr.join(''));
  } else if (base === 2) {
    const binaryArray = number.toString().split('').reverse();
    let decimal = 0;

    for (let i = 0; i < binaryArray.length; i++) {
      const element = binaryArray[i] * 2 ** i;
      decimal += element;
    }
    return decimal;
  }
};

console.log(conversion(294));
