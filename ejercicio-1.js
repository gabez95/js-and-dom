/**
 * Ejercicio 1
Escribe una función que devuelva un array de usuarios. De cada usuario guardaremos el username, el nombre y apellido, el sexo, el país, el email y un enlace a su foto de perfil.
El número de usuarios a obtener se debe indicar con un parámetro de dicha función.
Sírvete de la API: https://randomuser.me/
 */

const getUsers = async (numberOfUsers) => {
  const array = [];
  try {
    // Peticion asincrona a la api
    const response = await fetch(
      `https://randomuser.me/api/?results=${numberOfUsers}`
    );

    // Parsear el json y extraer el array results por destructuring
    const { results } = await response.json();

    for (const user of results) {
      // Extrayendo las propiedades del objeto
      const username = user.login.username;
      const { first, last } = user.name;
      const country = user.location.country;
      const gender = user.gender;
      const img = user.picture.large;
      const email = user.email;

      // Introduciendo nuevo objeto usuario al array resultante
      array.push({
        usuario: username,
        nombre: first,
        apellido: last,
        pais: country,
        genero: gender,
        imagen: img,
        correo_electronico: email,
      });
    }
    return array;
  } catch (error) {
    console.error(error);
  }
};

console.log(getUsers(2));
