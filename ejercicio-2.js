/**
 * Ejercicio 2
Crea un programa que imprima cada 5 segundos el tiempo desde la ejecución del mismo. Formatea el tiempo para que se muestren los segundos, los minutos, las horas y los días desde la ejecución.
 */

const cronometro = () => {
  let startTime = new Date();
  let sec = 0;
  let min = 0;
  let hour = 0;
  let day = 0;
  setInterval(() => {
    let actualTime = new Date();
    let timeElapsed = actualTime - startTime;

    sec = Math.round(timeElapsed / 1000);

    if (sec === 60) {
      actualTime = new Date();
      startTime = new Date();
      sec = 0;
      min++;
    }
    if (min === 60) {
      min = 0;
      hour++;
    }
    if (hour === 60) {
      hour = 0;
      day++;
    }

    console.log(
      `Han pasado ${day} días, ${hour} horas, ${min} minutos y ${sec} segundos`
    );
  }, 5000);
};

cronometro();
