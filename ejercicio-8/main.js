// DOM elements

let millisecondDisplay = document.querySelector('.millisecond');
let secondDisplay = document.querySelector('.second');
let minuteDisplay = document.querySelector('.minute');
const startButton = document.querySelector('#start');
const pauseButton = document.querySelector('#pause');
const resetButton = document.querySelector('#reset');

// Chronometer elements
let second = 0;
let minute = 0;
let temp = 0;

//interval
let countingLoop;

startButton.addEventListener('click', (e) => {
  e.preventDefault();

  if (!startButton.disabled) startButton.disabled = true;

  if (pauseButton.classList.contains('paused')) {
    pauseButton.classList.remove('paused');
  }

  let startTime = new Date();

  countingLoop = setInterval(() => {
    let actualTime = new Date();

    second = Math.round((actualTime - startTime) / 1000) + temp;

    if (second === 60) {
      actualTime = new Date();
      startTime = new Date();
      second = 0;
      temp = 0;
      minute++;
    }

    second < 10
      ? (secondDisplay.textContent = `0${second}`)
      : (secondDisplay.textContent = second);

    minute < 10
      ? (minuteDisplay.textContent = `0${minute}`)
      : (minuteDisplay.textContent = minute);
  }, 1000);
});

pauseButton.addEventListener('click', (e) => {
  e.preventDefault();

  // If count has started...
  if (startButton.disabled) {
    if (!pauseButton.classList.contains('paused')) {
      clearInterval(countingLoop);

      pauseButton.classList.add('paused');
      startButton.disabled = false;
    }
    temp = second;
  }
});

resetButton.addEventListener('click', (e) => {
  e.preventDefault();
  clearInterval(countingLoop);

  startButton.disabled = false;

  pauseButton.classList.contains('paused')
    ? pauseButton.classList.remove('paused')
    : pauseButton;

  minute = 0;
  second = 0;
  temp = 0;
  minuteDisplay.textContent = '00';
  secondDisplay.textContent = '00';
});
